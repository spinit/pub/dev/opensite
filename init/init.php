<?php
use Spinit\Dev\MRoute\Core\InstanceRegister;
use Spinit\Dev\Opensymap\Core\ApplicationAdapterRegister;
use Spinit\Dev\Opensymap\Adapter\Xml\ApplicationAdapterXml;
use Spinit\Dev\Opensite\Opensite;
use Spinit\Util;

InstanceRegister::setInstanceManager(Opensite::class, 'Open Site', 1, 1);
if (class_exists(Util\getClassPath("Spinit:Dev:Opensymap:Core:ApplicationAdapterRegister"))) {
    ApplicationAdapterRegister::set(new ApplicationAdapterXml(dirname(__DIR__).'/app/'));
}
