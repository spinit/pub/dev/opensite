<?php

namespace Spinit\Dev\Opensite;

use Spinit\Util\Error\NotFoundException;

class ArticleController {
    private $art;

    public function __construct($art) {
        $this->art = $art;
    }

    public function getArticle() {
        $args = func_get_args();
        if (!count($args)) {
            return $this->art;
        }
        return call_user_func_array([$this->art, 'get'], $args);
    }
    public function getSite() {
        return $this->getArticle()->getSite();
    }
    protected function isFound()
    {
        // se l'id non è impostato ... allora non è stato trovato
        if (!$this->getArticle('id')) {
            throw new NotFoundException();
        }
    }
    protected function isPathOK()
    {
        // se è stato trovato ma il path richiesto non è esatto ... 
        if ($this->getArticle('arg')) {
            throw new NotFoundException('Troppo lungo');
        }
    }
    
    public function check() {
        $this->isFound();
        $this->isPathOK();
    }

    public function makeBody() {
        $tmpl = $this->getArticle('tmpl_item')?:$this->getArticle('tmpl_main');
        $body = $this->getArticle('dsc');
        $site = $this->getArticle()->getSite();
        if ($tmpl) {
            $rec = $site->getSectionItem(['id'=>$this->getArticle('id_lng')], $tmpl)->first();
            if ($rec) {
                $body = $rec['dsc'];
            }
        }      
        return $body;  
    }
}