<?php

namespace Spinit\Dev\Opensite;

use function Spinit\Util\asArray;

class PathConf {

    private $path;
    private $lang;
    private $site;

    public function __construct($site, $path) {
        $this->site = $site;  
        $this->makePath($path);
    }

    public function getLang() {

    }

    public function getPath() {

    }

    public function makePath($path) {
        $lang = $this->site->getInstance()->getEnv('opensite:lang');
        $path = asArray($path, '/');
        if (!$lang) {}
    }
}