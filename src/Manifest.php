<?php

namespace Spinit\Dev\Opensite;

use Spinit\Util\DictionaryBase;
use Webmozart\Assert\Assert;

use function Spinit\Util\file_get_contents;
use function Spinit\Util\nvl;

class Manifest {
    private $site;
    private $dat;
    public function __construct(Site $site) {
        $this->site = $site;
        $fname = $site->getThemeRoot('manifest.json');
        Assert::fileExists($fname, 'ERRORE : manifest.json not found ['.$fname.']');
        $this->dat = new DictionaryBase(json_decode(file_get_contents($fname), 1));
    }

    public function getSite() {
        return $this->site;
    }

    public function get() {
        $args = func_get_args();
        return call_user_func_array([$this->dat, 'get'], $args);
    }

    public function set() {
        $args = func_get_args();
        return call_user_func_array([$this->dat, 'set'], $args);
    }

    public function getAssetList($layout, $type) {
        $root = $this->getSite()->getThemeRoot($this->getAssetRoot());
        yield from $this->getResource($root, $type, $this->dat->get('asset.common'));
        yield from $this->getResource($root, $type, $this->dat->get('asset.layout.'.($layout?:'index')));
    }

    protected function getResource ($root, $type, $list) {
        foreach($list?:[] as $res) {
            if (substr($res, 0, 2) == '//' or strpos($res, '://')) {
                $info = pathinfo($res);
                if ($info['extension'] == $type) {
                    yield $res;
                }
            } else {
                yield from $this->getAllResource($root, $type, $res);
            }
        }
    }

    protected function getAllResource($dir, $type, $all) {
        $root = fspath($dir, $all);
        if (!is_dir($root)) {
            yield from $this->getOneResource($dir, $type, $all);
        } else {
            foreach(scandir($root) as $file) {
                if (in_array($file, ['.', '..'])) continue;
                yield from $this->getAllResource($dir, $type, fspath($all, $file));
            }
        }
    }

    protected function getOneResource($dir, $type, $file) {
        $staType = '.'.$type;
        $dynType = $staType .'.php';
        if (substr($file, -strlen($staType)) == $staType) {
            $rname = $file;
            $fileList = [$file, $file.'.php'];
        } else if (substr($file, -strlen($dynType)) == $dynType) {
            $rname = substr($file, 0, -4);
            $fileList = [$file];
        } else {
            return;
        }
        foreach($fileList as $item) {
            if (is_file(fspath($dir, $item))) {
                yield($rname);
                break;
            }
        }
    }

    public function getAssetRoot() {
        return $this->get('asset.root', 'asset');
    }

    public function getAssetRoute() {
        return $this->get('asset.route', 'asset');
    }

    public function makeUrl($res) {
        if (substr($res, 0, 2) == '//' or strpos($res, '://')) {
            return $res;
        }
        $site = $this->getSite();
        return $site->getInstance()->makePath($this->getAssetRoute().'/'.$res);
    }

    public function getContent($res) {
        $fname = $this->getSite()->getThemeRoot(fspath($this->getAssetRoot(), $res));

        if (!is_file($fname)) $fname .= '.php';
        if (!is_file($fname)) return;

        $info = pathinfo($fname);
        if ($info['extension'] == 'php') {
            ob_start();
            require $fname;
            return ob_get_clean();
        }
        return file_get_contents($fname);
    }

    public function echoAllCss($layout) {
        foreach($this->getAssetList($layout, 'css') as $res) {
            echo "/**\n";
            echo " ** ".$this->makeUrl($res)."\n";
            echo " **/\n\n";
            echo $this->getContent($res)."\n";
            echo "\n\n";
        }
    }
}