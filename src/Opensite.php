<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Spinit\Dev\Opensite;

use Spinit\Dev\MRoute\Core\Exception\ResponseException;
use Spinit\Dev\MRoute\Instance;

/**
 * Description of Opensite
 *
 * @author ermanno
 */

class Opensite extends Instance {
    //put your code here
    public function start() {
        $site = $this->getSite($this->getEnv('opensite:group'), $this->getEnv('opensite:name'));
        $page = $site->getPage();
        try {
            return $page->run()?:$this->getResponse();
        } catch (ResponseException $e) {
            return $e->getResponse();
        }
    }

    public function getSite($group,$name) {
        $site = new Site($this, ['grp'=>$group, 'nme'=>$name]);
        return $site;
    }
}
