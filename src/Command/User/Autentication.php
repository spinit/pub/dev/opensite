<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Spinit\Dev\Opensite\Command\User;

use Spinit\Dev\MRoute\Core\CommandInstance;
use Spinit\Util\Error\NotFoundException;

/**
 * Description of Autentication
 *
 * @author ermanno
 */
class Autentication extends CommandInstance {
    //put your code here
    public function exec($param = null) {
        $args = func_get_args();
        $sql =  "
        select hex(a.id) as id, a.nme as name, /*hex(aut_role)*/ 'admin' as aut_role, 
           hex(r.rel_id) as url_rel, hex(a.aut_url_id) as url_mst
        from osy_ang a
        left join
        (
            select r.*
            from osy_ang__rel r
            inner join osy_itm rt on (r.id_typ = rt.id and rt.urn = 'urn:opensymap.org:user-rel#instance-url')
            inner join @DataSource.main@.osx_ice_url u on (r.rel_id = u.id and u.act = 1 and u.dat_del__ is null)
        ) r on  (r.id_ang = a.id)
        left join @DataSource.main@.osx_ice_url up on (up.id_par = a.aut_url_id and up.id = {{@id_url}} and up.dat_del__ is null)
        where a.aut_lgn = {{login}}
          and a.aut_pwd = {{@passwd}}
          and {{@id_url}}  in (a.aut_url_id, r.rel_id, up.id) 
          and a.dat_del__ is null
          and a.aut_act = '1'
          and coalesce(a.aut_pwd, '') != ''";
        $rs = $this->getDataSource()
                     ->query($this->getInstance()->normalizeQuery($sql),
                           ['login' => $args[0], 'passwd'=>md5($args[1]), 'id_url'=>$this->getInstance()->getInfo('id_url')]);
        //debug($rs, $this->getInstance()->getInfo());
        $user = $rs->first();
        if (!$user) {
            throw new NotFoundException('Credenziali non valide');
        }
        // l'utente è stato riconosciuto nella url corrente, che è la principale oppure un'alias.
        // In entrambi i casi la url principale è presente in url_mst
        $url_mst = $user['url_mst'];
        // a questo punto occorre vedere se l'utente può andare ovunque o ha un accesso limitato
        $sql = "
            select hex(oiu.id) as id
            from @DataSource.main@.osx_ice_url oiu 
            inner join osy_ang__rel ur on (oiu.id = ur.rel_id and ur.id_ang = {{@id}})
            where {{@url_mst}} in (oiu.id_par)
        ";
        $rs = $this->getDataSource()->query($this->getInstance()->normalizeQuery($sql), $user);
        $user['url_list'] = $rs->getList();
        //debug($user, $this->getInstance()->getInfo());
        if (count($user['url_list'])) {
            // l'utente ha un accesso limitato ... occorre vedere se la url corrente è tra quelle visibili
            if (!in_array($this->getInstance()->getInfo('id_url'), $user['url_list'])) {
                throw new NotFoundException('Accessono non acconsentito');
            }
        }
        // altrimenti può accedere
        return $user;
    }

}

