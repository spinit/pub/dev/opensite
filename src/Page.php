<?php

namespace Spinit\Dev\Opensite;

use Spinit\Dev\MRoute\Core\Assets;
use Spinit\Dev\MRoute\Core\Exception\ResponseException;
use Spinit\Dev\MRoute\Core\Response;
use Spinit\Dev\MRoute\Core\Router;
use Spinit\Dev\MRoute\Helper\ResourceLoader;
use Spinit\Dev\Opensite\Maker\Content;
use Spinit\Dev\Opensymap\Helper\FileNotFoundImage;
use Spinit\Util\Error\NotFoundException;
use Spinit\Dev\MRoute\Core\Page as MPage;
use Spinit\Dev\Opensite\Helper\ArticleMakeLink;
use Webmozart\Assert\Assert;
use Spinit\Dev\Opensite\Helper\AccessException;

use function Spinit\Util\normalize;
use function Spinit\Util\arrayGet;
use function Spinit\Util\asArray;


class Page {

    private $site;
    private $path;
    private $router;
    private $baseRoot = '__base';
    private $layout = 'index';
    private $maker;
    private $hasLang = false;

    public function __construct($site, $path) {
        $this->site = $site;
        $this->path = $path;
        $this->router = new Router($site->getInstance());
        $this->router->set($site->getManifest()->getAssetRoute(), [$this, 'getAssetRunner']);
        $this->router->set($this->baseRoot, [$this, 'getAssetBase']);
        $this->router->set('', [$this, 'getDefaultRunner']);
        $this->maker = new Content($this);
    }

    public function getSite() {
        return $this->site;
    }

    public function setLayout($layout) {
        $this->layout = $layout;
        return $this;
    }
    public function getLayout() {
        return $this->layout;
    }
    public function getInstance() {
        return $this->getSite()->getInstance();
    }
    
    private function makeLangAndPath($path) {
        $instance = $this->site->getInstance();
        $lng = $instance->getLang($this->getSite()->getInstance()->getInfo('id_lng'));
        $fix_lng = $this->getSite()->getInstance()->getInfo('fix_url_lng');
        $path = asArray($path, '/');
        // la lingua è impostata sulla url?
        // se non è fixata viene controllato che il primo elemento corrisponda a quanto previsto
        if ($lng) {
            if (!$fix_lng and count($path)) {
                $flng = false;
                // se è stato impostato un alias web della lingua direttamente sulla url
                if ($web = $instance->getEnv('opensite:lang:web') and $web == $path[0]) {
                    array_shift($path);
                    $lng['name'] = $web;
                } else {
                    // altrimenti viene ricercato tra gli alias impostati sulla lingua stessa
                    $flng = $instance->getLang($path[0]);
                    if ($flng and $flng['id'] == $lng['id']) {
                        array_shift($path);
                    }
                }
            }
            $this->hasLang = true;
            return [$flng ?: $lng, $path];
        }
        if (count($path)) {
            $lng = $instance->getLang($path[0]);
            $lng and array_shift($path);
        }
        (!$lng) and $lng = $instance->getLang($this->getSite()->get('id_dfl_lng'));
        $this->hasLang = !!$lng;

        (!$lng) and $lng = $instance->getLang($instance->getEnv('opensite:lang:default'));
        (!$lng) and $lng = $instance->getLang();
        
        return [$lng, $path];
    }

    public function getLang($what = '') {
        switch($what) {
            case 'id':
                return arrayGet($this->lang, 0);
            case 'name':
                return arrayGet($this->lang, 1);
        }
        return $this->lang;
    }

    public function getPath() {
        return $this->path;
    }

    public function getResponse() {
        return $this->site->getInstance()->getResponse();
    }

    public function run() {
        return $this->router->run($this->path);
    }

    public function getAssetRunner($path, $returnContent = 0) {
        $site = $this->site;
        $manifest = $site->getManifest();
        $asset = new Assets($site->getInstance(), $this->getSite()->getThemeRoot($manifest->getAssetRoot()), $manifest->getAssetRoute());
        $content = $asset->run($path);
        if ($returnContent) {
            return $content;
        }
        $this->getResponse()->setContent($content);
    }

    public function getAssetBase($path) {
        $path = asArray($path, '/');
        $pag = ['id' => array_shift($path), 'id_sit'=>$this->site->get('id')];
        $this->getSite()->showResource($path, $pag);
    }

    public function getDefaultRunner($path) {
        $response = $this->getResponse();
        
        list($lang, $subpath) = $this->makeLangAndPath($path);
        $this->site->setLang($lang);
        $response->set('page.lang', $lang);
        $response->set('page.root', $this->getInstance()->makePath('/'));
        $response->set('page.asset', $this->getInstance()->makePath($this->site->getManifest()->getAssetRoute()));
        $art = null;

        // la pagina è presente nel tema?
        try {
            $page = new MPage($this->getInstance(), $this->getSite()->getThemeRoot());
            $page->setParam('page', $this);
            $page->run($subpath);
        } catch (NotFoundException $e) {
            // se non è presente .. viene ricercata la pagina nel DB
            $art = $this->makeArticle($lang, $subpath);
        }
        $this->setResponseContent($art);
    }

    private function makeArticle ($lang, $subpath) {
        $art = null;
        try {
            $art = $this->site->getArticle($lang, $subpath);
            $response = $this->getResponse();
            $response->set('page.url', $this->getSite()->makePath(implode('/', $subpath)));    
            $response->set('page.title', $art->get('lbl'));    
            $this->layout = $art->get('lay', 'index');
            $response->set('page.base', $this->getInstance()->makePath($this->baseRoot.'/'.$art->get('id').'/'));
            $art->set('page', $response->get('page'));
            
            // bisogna usare un template per il documento?
            $tmpl = $art->get('tmpl_item')?:$art->get('tmpl_main');
            // se si ... viene caricato
            // prima vengono processati i maker presenti nel documento
            $content = $this->maker->make($art, [], $art->get('body'));
            // vengono sostituite quindi le variabili del documento
            list($content, ) = normalize($content, $art->get());
            $response->set('page.content', $content);
            $response->set('page.name', $art->get('fnme'));
            $response->set('page.style', $art->get('sty'));
        } catch (ResponseException $e) {
            throw $e;
        } catch (AccessException $e) {
            $page = new MPage($this->getInstance(), $this->getSite()->getThemeRoot());
            $page->setParam('page', $this);
            $response = $page->run('login');

            if ($response->getHeader('Content-Type') == 'text/html') {
                $response->set('page.content', $this->maker->make([], [], $response->get('page.content')));
            }
        } catch (\Exception $e) {
            $this->layout = 'not-found';
        }
        return $art;
    }
    private function setResponseContent($art) {
        $response = $this->getResponse();
        switch($this->site->getInstance()->getRequest()->getMethod()) {
            case 'GET':
                $filename = $this->site->getThemeRoot("layout/{$this->getLayout()}.php");
                if (!is_file($filename)) {
                    $filename = $this->site->getThemeRoot("layout/index.php");
                }
                Assert::fileExists($filename, 'Errore : Layout non trovato ['.$filename.']');
                ob_start();
                require ($filename);
                $content = $this->maker->make($response, [], ob_get_clean());
                $content = $this->replaceLinkInContent($art, $content);
                $response->setContent($content, 'text/html');
                break;
        }
    }

    private function replaceLinkInContent($art, $content) {
        if (!$art) {
            return $content;
        }
        Assert::notNull($art);
        preg_match_all('/"@([^ @\"\']+)([@\"\' ])/', $content, $matches);
        
        $pageList = $matches[0];
        rsort($pageList);
        //debug($pageList);
        foreach($pageList as $k=>$aref) {
            $href = trim($aref, '"');
            $part = asArray($href, ':');
            $sname = $this->getSite()->getInstance()->getEnv('opensite:name');
            if (count($part)>1) {
                $sname = ltrim(array_shift($part), '@');
            }
            $path = ltrim(array_shift($part), '@');
            if (substr($path, 0, 2) == './') {
                $path = ArticleMakeLink::path($art->get('fnme'), substr($path, 2));
            }
            //debug($sname, $path);
            $link = $this->getSite()->getPagePath($sname, $path, $art->get('id_lng'));
            $content = str_replace($href, $link, $content);
        }
        return $content;
    }
}
