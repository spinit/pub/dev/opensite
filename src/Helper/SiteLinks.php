<?php

namespace Spinit\Dev\Opensite\Helper;

use Spinit\Dev\MRoute\Core\HasInstance;
use Spinit\Dev\MRoute\Core\Type\InstanceInterface;
use Spinit\Dev\Opensite\Opensite;
use function Spinit\Util\arrayGet;
use function Spinit\Util\asArray;
use function Spinit\Util\getColonsPath;

class SiteLinks {

    use HasInstance;

    private $groupName;
    private $siteName;
    private $lang;
    private $links = [];
    private $base = '';
    private $dom = '';
    public function __construct(InstanceInterface $instance, $groupName, $siteName, $lang) 
    {
        $this->setInstance($instance);
        $this->groupName = $groupName;
        $this->siteName = $siteName;
        $this->lang = $lang;
        $this->dom = $this->getInstance()->getInfo('url');
        //debug($this->getInstance()->getInfo());
        $sit = $this->getSiteInfo();
        $this->makeLinks($sit);
    }

    public function get() {
        $args = func_get_args();
        array_unshift($args, $this->links);
        $link = '//'.trim($this->base, '/').'/'.call_user_func_array('\\Spinit\\Util\\arrayGet', $args);
        return $link;
    }

    public function make($url) {
        $link = '//'.trim($this->base, '/').'/'.$url;
        return $link;
    }

    private function getSiteInfo() {
        // quali url sono configurate con il sito
        $sql = "
            select hex(d.id) as id, d.url, locate(d.url, {{dom}}) as loc, d.id_par, hex(ss.id) as id_sit, hex(d.id_lng) as has_lng, fix_url_lng ,
                (select case when coalesce(als, '') = '' then cod else als end from osy_itm where id = {{@lng}}) as cod_lng, 
                d.wlng
            from (
                select mm.id, mm.url, mm.id_lng, meg.val grp, mes.val as sit, '' id_par, mm.fix_url_lng, 
                mm.als_lng as wlng
                from @DataSource.main@.osx_ice_url mm
                left join osy_env meg on (mm.id = meg.id_url and meg.nme='opensite:group')
                left join osy_env mes on (mm.id = mes.id_url and mes.nme='opensite:name')
                where mm.id_par is null 
                  and mm.manager = {{mng}}
                  and mm.act = '1'
                  and mm.dat_del__ is null
                union
                select m1.id, m1.url, coalesce(m1.id_lng, mm.id_lng) as id_lng,
                        coalesce(m1g.val, meg.val) as grp,
                        coalesce(m1s.val, mes.val) as sit,
                        hex(mm.id) as id_par,
                        case when m1.id_lng is null then mm.fix_url_lng  else m1.fix_url_lng end,
                        coalesce(m1.als_lng, mm.als_lng) as  wlng
                from @DataSource.main@.osx_ice_url mm
                left join @DataSource.main@.osx_ice_url m1 on (m1.id_par = mm.id and m1.act = '1' and m1.dat_del__ is null)
                left join osy_env meg on (mm.id = meg.id_url and meg.nme='opensite:group')
                left join osy_env mes on (mm.id = mes.id_url and mes.nme='opensite:name')
                left join osy_env m1g on (m1.id = m1g.id_url and m1g.nme='opensite:group')
                left join osy_env m1s on (m1.id = m1s.id_url and m1s.nme='opensite:name')
                where mm.id_par is null
                  and mm.act = '1'
                  and mm.manager = {{mng}}
                  and mm.dat_del__ is null
            ) d
            inner join opn_sit ss on (d.grp = ss.grp and d.sit = ss.nme and ss.dat_del__ is null)
            where d.grp = {{grp}}
            and sit = {{nme}}
            and (id_lng is null or id_lng = {{@lng}})
            order by 
                case when locate(d.url, {{dom}}) = 0 then 1 else 0 end,
                case when id_lng is null then 1 else 0 end,
                length(url) desc
        ";
        $rs = $this->getDataSource()->query($this->getInstance()->normalizeQuery($sql), [
            'grp' => $this->groupName, 
            'nme'=>$this->siteName, 
            'lng'=>$this->lang, 
            'dom'=>$this->dom,  
            'mng'=>getColonsPath(Opensite::class)
        ]);
        //debug($rs);
        return $rs->first();
    }

    private function makeLinks($sit) {
        $sit['id_lng'] = $this->lang;
        //debug($sit);
        $this->base = rtrim($sit['url'], '/');
        //debug($sit);
        //if ((!$sit['has_lng'] or !$sit['fix_url_lng']) and $sit['cod_lng']) {
            //debug($sit);
        $cod_lng = $sit['wlng'] ?: $sit['cod_lng'];
        if (!$sit['fix_url_lng'] and $cod_lng) {
            $this->base .= '/'.$cod_lng;
        }
        $sql = "
        select a.fnme, l.slug, hex(l.id) as lid
        from opn_sit_itm a
        inner join osy_itm t on (a.id_typ = t.id and t.urn = 'urn:opensite.org/item@type#page')
        left join opn_sit_itm l on (l.id_fst = a.id_fst and l.dat_del__ is null and l.id_lng = {{@id_lng}})
        where a.id = a.id_fst
          and a.dat_del__ is null
          and a.id_sit = {{@id_sit}}
        order by concat(a.fnme, '/')
        ";
        $list = $this->getDataSource()->query($sql, $sit);
        //debug($list);
        foreach($list as $item) {
            if (!$item['lid']) {
                // traduzione non trovata
                continue;
            }
            $part = asArray($item['fnme'], '/');
            array_pop($part);
            $pnme = implode('/', $part);
            $slug = [];
            if ($pnme and !array_key_exists($pnme, $this->links)) {
                // parent non definito?
                continue;
            } 
            if ($pslug = rtrim(arrayGet($this->links, $pnme), '/')) {
                $slug []= $pslug;
            }
            if ($item['slug']) {
                $slug [] = $item['slug'];
                $slug [] = '';
            }
            $this->links[$item['fnme'].''] = implode('/', $slug);
        }
    }
}
