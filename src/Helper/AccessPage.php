<?php

namespace Spinit\Dev\Opensite\Helper;

use Spinit\Dev\MRoute\Core\HasInstance;
use Spinit\Dev\MRoute\Core\Type\InstanceInterface;
use Spinit\Dev\Opensite\Opensite;
use function Spinit\Util\arrayGet;
use function Spinit\Util\getColonsPath;

class AccessPage {

    use HasInstance;

    private $pag;
    private $maxLevel = 10;

    public function __construct(InstanceInterface $instance, $pag)
    {
        $this->setInstance($instance);
        $this->pag = $pag;    
    }

    /**
     * Se l'utente è loggato non occorre effettuare controlli
     * altrimenti occorre verificare che la pagina chiamata sia pubblica
     * (per essere pubblica occorre che il sito non sia privato + che la pagiana non sia privata + che non abbia un parent privato)
     */
    public function check() {
        if ($this->getInstance()->hasUser()) {
            $user = $this->getInstance()->getUser();
            if (!$user['url_list'] or in_array($this->getInstance()->getInfo('id_url'), $user['url_list'])) {
                return true;
            }
            return false;
        }
        $lsql = [];
        for($i = 0; $i < $this->maxLevel; $i++) {
            $lsql [] = $this->getSqlLevel($i);
        }
        $sql = "select count(*) cc from (
            ".implode("\n UNION \n", $lsql)."
            ) d 
            where '1' in (d.sit_pri, d.pag_pri)";
        $rs = $this->getDataSource()->query($sql, $this->pag);
        return !$rs->first('cc');
    }

    private function getSqlLevel($depth) {
        $js = ["opn_sit_itm pm on (pl.id_fst = pm.id and pm.dat_del__ is null and pm.id_sit = {{@id_sit}})", "opn_sit s on (pm.id_sit = s.id)"];
        $pm = 'pm';
        for($i = 0; $i < $depth; $i++) {
            $pmn = "pm{$i}";
            $js []= "opn_sit_itm {$pmn} on ({$pm}.id_par = {$pmn}.id and {$pmn}.dat_del__ is null)";
            $pm = $pmn;
        }
        
        $sql = "
            select s.is_pri as sit_pri, {$pm}.is_pri pag_pri
            from opn_sit_itm pl
            inner join ".implode("\n    inner join ", $js)."
            where pl.id = {{@id}}
              and {$pm}.id_par is null
        ";
        return $sql;
    }
}