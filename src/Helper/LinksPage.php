<?php

namespace Spinit\Dev\Opensite\Helper;

use Spinit\Dev\MRoute\Core\HasInstance;
use Spinit\Dev\Opensite\Opensite;
use function Spinit\Util\getColonsPath;

class LinksPage {

    use HasInstance;
    private $site;
    private $pag;
    private $maxLevel = 10;
    private $links = [];

    public function __construct($site, $pag)
    {
        $this->setInstance($site->getInstance());
        $this->site = $site;
        $this->pag = $pag;    
        $this->base = $this->getSiteInfo();
        $this->links = $this->makeLinks();
    }

    /**
     * Se l'utente è loggato non occorre effettuare controlli
     * altrimenti occorre verificare che la pagina chiamata sia pubblica
     * (per essere pubblica occorre che il sito non sia privato + che la pagiana non sia privata + che non abbia un parent privato)
     */
    private function getSiteInfo() {
        // quali url sono configurate con il sito
        $sql = "
            select hex(d.id_lng) as id, d.url
            from (
                select mm.id, mm.url, mm.id_lng, meg.val grp, mes.val as sit, '' id_par
                from @DataSource.main@.osx_ice_url mm
                left join osy_env meg on (mm.id = meg.id_url and meg.nme='opensite:group')
                left join osy_env mes on (mm.id = mes.id_url and mes.nme='opensite:name')
                where mm.id_par is null 
                and mm.manager = {{mng}}
                union
                select m1.id, m1.url, coalesce(m1.id_lng, mm.id_lng) as id_lng,
                        coalesce(m1g.val, meg.val) as grp,
                        coalesce(m1s.val, mes.val) as sit,
                        hex(mm.id) as id_par
                from @DataSource.main@.osx_ice_url mm
                left join @DataSource.main@.osx_ice_url m1 on (m1.id_par = mm.id)
                left join osy_env meg on (mm.id = meg.id_url and meg.nme='opensite:group')
                left join osy_env mes on (mm.id = mes.id_url and mes.nme='opensite:name')
                left join osy_env m1g on (m1.id = m1g.id_url and m1g.nme='opensite:group')
                left join osy_env m1s on (m1.id = m1s.id_url and m1s.nme='opensite:name')
                where mm.id_par is null
                and mm.manager = {{mng}}
            ) d
            inner join opn_sit ss on (d.grp = ss.grp and d.sit = ss.nme and ss.dat_del__ is null)
            where d.grp = {{grp}}
              and d.sit = {{nme}}
        ";
        $rec = $this->getDataSource()->query($this->getInstance()->normalizeQuery($sql), [
            'grp' => $this->site->get('grp'), 
            'nme'=>$this->site->get('nme'), 
            'mng'=>getColonsPath(Opensite::class)
        ]);

        //debug($rec);

        return $rec->getList();
    }

    private function makeLinks() {
        $lsql = [];
        for($i = 0; $i < $this->maxLevel; $i++) {
            $lsql [] = $this->getSqlLevel($i);
        }
        $sql = implode("\n UNION \n", $lsql);
        $rec = $this->getDataSource()->query($sql, $this->pag);
        debug($rec);
        return !$rec['cc'];
    }

    private function getSqlLevel($depth) {
        $js = ["opn_sit_itm pm on (pl.id_fst = pm.id and pm.dat_del__ is null and pm.id_sit = {{@id_sit}})", "opn_sit s on (pm.id_sit = s.id)"];
        $pm = 'pm';
        for($i = 0; $i < $depth; $i++) {
            $pmn = "pm{$i}";
            $js []= "opn_sit_itm {$pmn} on ({$pm}.id_par = {$pmn}.id and {$pmn}.dat_del__ is null)";
            $pm = $pmn;
        }
        
        $sql = "
            select s.is_pri as sit_pri, {$pm}.is_pri pag_pri
            from opn_sit_itm pl
            inner join ".implode("\n    inner join ", $js)."
            where pl.id = {{@id}}
              and {$pm}.id_par is null
        ";
        return $sql;
    }
}