<?php

namespace Spinit\Dev\Opensite\Helper;

use Spinit\Dev\MRoute\Core\HasInstance;
use Spinit\Dev\MRoute\Core\Type\InstanceInterface;
use Spinit\Dev\Opensite\Article;
use Spinit\Dev\Opensite\Opensite;
use function Spinit\Util\arrayGet;
use function Spinit\Util\asArray;
use function Spinit\Util\getColonsPath;

class ArticleFromLink {

    use HasInstance;

    private $site;

    public function __construct($site) 
    {
        $this->setInstance($site->getInstance());
        $this->site = $site;
    }

    public function getArticle($link, $lang) {

        $maxLevel = count(asArray($link, '/')) + 1;
        $par = ['id_lng' => $lang['id'], 'id_sit'=>$this->site->get('id'), 'path' => implode('/', $link)];
        $lsql = [];
        for($i = 0; $i < $maxLevel; $i++) {
            $lsql []= $this->getLinksQuery($i);
        }
        $sql = "
            select hex(a.id) as id, hex(a.id_fst) as id_fst, hex(a.id_sit) as id_sit, 
                hex(a.id_lng) as id_lng, hex(a.id_typ) as id_typ, 
                case when coalesce(lng.als, '') = '' then lng.cod else lng.als end as lng,
                d.nme, d.pat, d.dep, trim(both '/' from substr({{path}}, length(d.pat)+1)) as arg,
                a.lbl, a.dsc, a.ctr, d.fnme, d.ctr,
                (select val_str from opn_sit_itm__p where id_mst = a.id_fst and nme_prp = 'page-template-main' limit 1) tmpl_main,
                (select val_str from opn_sit_itm__p where id_mst = a.id and nme_prp = 'page-template-item' limit 1) tmpl_item,
                (
                    select is_pri from opn_sit_itm p
                    where p.id_typ = a.id_typ
                    and p.dat_del__ is null
                    and is_pri = 1
                    and locate(p.fnme, d.fnme)
                    order by length(p.fnme) DESC 
                    limit 1
                ) as is_pri,
                (
                    select lay from opn_sit_itm p
                    where p.id_typ = a.id_typ
                    and p.dat_del__ is null
                    and coalesce(p.lay, '') != ''
                    and locate(p.fnme, d.fnme)
                    order by length(p.fnme) DESC 
                    limit 1
                ) as lay
            from (
                ".implode("\n UNION \n", $lsql)."
            ) d
            inner join opn_sit_itm a on (d.id = a.id)
            left join osy_itm lng on (a.id_lng = lng.id)
            where locate(concat(d.pat, '/'), '{{:path}}/')
            order by length(d.pat) desc";

        $rs = $this->getDataSource()->query($sql, $par);
        return new Article($this->site, $rs->first());
    }

    private function getLinksQuery($depth) {

        $lj = ["opn_sit_itm pl on (pl.id_fst = pm.id and pl.id_lng = {{@id_lng}} and pl.dat_del__ is null)"];
        $lnme = ["pm.nme"];
        $lslug = ["pl.slug"];
        $pm = 'pm';
        $pl = 'pl';
        for($i = 0; $i<$depth; $i++) {
            $pmn = "pm{$i}";
            $pln = "pl{$i}";
            $lj []= "opn_sit_itm {$pmn} on ({$pmn}.id_par = {$pm}.id and {$pmn}.id = {$pmn}.id_fst and {$pmn}.dat_del__ is null and {$pmn}.id_typ = pm.id_typ)";
            $lj []= "opn_sit_itm {$pln} on ({$pln}.id_fst = {$pmn}.id and {$pln}.id_lng = {{@id_lng}} and {$pln}.dat_del__ is null)";
            $lnme[] = $pmn.'.nme';
            $lslug[] = $pln.'.slug';
            $pm = $pmn;
            $pl = $pln;
        }
        $sql = "
            select  {$pl}.id as id, concat_ws('/', ".implode(', ', $lnme).") as nme, {$pm}.fnme,
                   concat_ws('/', ".implode(', ', $lslug).") as pat, {$depth} as dep, pm.ctr
            from opn_sit_itm pm
            inner join osy_itm pt on (pm.id_typ = pt.id and pt.urn = 'urn:opensite.org/item@type#page')
            inner join ".implode("\n    inner join ", $lj)."
            where pm.id_sit = {{@id_sit}}
              and pm.id_par is null
              and pm.dat_del__ is null
              and pm.id = pm.id_fst
        ";
        return $sql;
    }
}
