<?php

namespace Spinit\Dev\Opensite\Helper;

use Spinit\Dev\MRoute\Core\HasInstance;
use Spinit\Dev\Opensite\Opensite;

use function Spinit\Util\arrayGet;
use function Spinit\Util\getColonsPath;

class ArticleMakeLink {

    use HasInstance;

    private $art;
    private $site;

    public function __construct($art) 
    {
        $this->setInstance($art->getSite()->getInstance());
        $this->art = $art;
        $this->site = $art->getSite();
    }

    public static function path() {
        $args = func_get_args();
        $end = '';
        if (substr($args[count($args)-1], -1) == '/') {
            $end = '/';
        }
        return rtrim(
            implode('/', array_filter(array_map(function($item) { return trim($item, '/');}, $args), 'strlen')),
            '/').$end;
    }
    public function getLinks() {

        $base = $this->getBaseList();
        $links = [];
        foreach($this->getLinkList() as $id_lng => $link) {
            $trs_lng = json_decode($link['trs_lng'], 1);
            $dom = arrayGet($base, $id_lng);
            if (!$dom) {
                $dom = self::path(arrayGet($base, ''), $link['cod_lng']);
            }
            $links[] = [
                'link'=>'//'.self::path($dom, $link['pat'].'/'),
                'id_lng'=>$id_lng,
                'name'=>arrayGet($trs_lng, ['nme', $link['cod_lng']], $link['nme_lng']),
                'res' => $link
            ];
        }
        return $links;
    }

    private function getBaseList() {
        $sql = "
            select coalesce(hex(d.id_lng), '') as id, d.url
            from (
                select mm.id, mm.url, mm.id_lng, meg.val grp, mes.val as sit, '' id_par
                from @DataSource.main@.osx_ice_url mm
                left join osy_env meg on (mm.id = meg.id_url and meg.nme='opensite:group')
                left join osy_env mes on (mm.id = mes.id_url and mes.nme='opensite:name')
                where mm.id_par is null 
                and mm.manager = {{mng}}
                and mm.act = '1'
                union
                select m1.id, m1.url, coalesce(m1.id_lng, mm.id_lng) as id_lng,
                        coalesce(m1g.val, meg.val) as grp,
                        coalesce(m1s.val, mes.val) as sit,
                        hex(mm.id) as id_par
                from @DataSource.main@.osx_ice_url mm
                left join @DataSource.main@.osx_ice_url m1 on (m1.id_par = mm.id and m1.act='1')
                left join osy_env meg on (mm.id = meg.id_url and meg.nme='opensite:group')
                left join osy_env mes on (mm.id = mes.id_url and mes.nme='opensite:name')
                left join osy_env m1g on (m1.id = m1g.id_url and m1g.nme='opensite:group')
                left join osy_env m1s on (m1.id = m1s.id_url and m1s.nme='opensite:name')
                where mm.id_par is null
                and mm.manager = {{mng}}
                and mm.act = '1'
            ) d
            inner join opn_sit ss on (d.grp = ss.grp and d.sit = ss.nme and ss.dat_del__ is null)
            where d.grp = {{grp}}
              and d.sit = {{nme}}
        ";
        $rs = $this->getDataSource()->query($this->getInstance()->normalizeQuery($sql), [
            'grp' => $this->site->get('grp'), 
            'nme'=>$this->site->get('nme'), 
            'mng'=>getColonsPath(Opensite::class)
        ]);
        //debug($rs);
        return $rs->getList();
       
    }
    private function getLinkList() {
        $par = ['id_sit'=>$this->art->getSite()->get('id'), 'id' => $this->art->get('id_fst'), 'id_lng'=>$this->art->get('id_lng')];
        $sql = $this->getLinksQuery($this->art->get('dep'));
        $rs = $this->getDataSource()->query($sql, $par);
        return $rs->getList();
    }

    private function getLinksQuery($depth) {

        $lj = ["opn_sit_itm pl on (pl.id_fst = pm.id and pl.dat_del__ is null and pl.id_lng != {{@id_lng}})",
               "osy_itm lng on (pl.id_lng = lng.id)"];
        $lnme = ["pm.nme"];
        $lslug = ["pl.slug"];
        $pm = 'pm';
        $pl = 'pl';
        for($i = 0; $i<$depth; $i++) {
            $pmn = "pm{$i}";
            $pln = "pl{$i}";
            $lj []= "opn_sit_itm {$pmn} on ({$pmn}.id_par = {$pm}.id and {$pmn}.id = {$pmn}.id_fst and {$pmn}.dat_del__ is null)";
            $lj []= "opn_sit_itm {$pln} on ({$pln}.id_fst = {$pmn}.id and {$pln}.id_lng = pl.id_lng and {$pln}.dat_del__ is null)";
            $lnme[] = $pmn.'.nme';
            $lslug[] = $pln.'.slug';
            $pm = $pmn;
            $pl = $pln;
        }
        $sql = "
            select hex(lng.id) as id_lng, 
                   hex({$pl}.id) as id, 
                   lng.cod as cod_lng,
                   lng.nme as nme_lng,
                   lng.trs as trs_lng,
                   trim(both '/' from concat_ws('/', ".implode(', ', $lslug).")) as pat, 
                   {$depth} as dep
            from opn_sit_itm pm
            inner join osy_itm pt on (pm.id_typ = pt.id and pt.urn = 'urn:opensite.org/item@type#page')
            inner join ".implode("\n    inner join ", $lj)."
            where pm.id_sit = {{@id_sit}}
              and {$pm}.id = {{@id}}
              and pm.id_par is null
              and pm.dat_del__ is null
              and pm.id = pm.id_fst
        ";
        return $sql;
    }
}