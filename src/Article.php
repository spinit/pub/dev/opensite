<?php

namespace Spinit\Dev\Opensite;

use Spinit\Dev\Opensite\Helper\ArticleMakeLink;
use Spinit\Dev\Opensite\Helper\LinksPage;
use Spinit\Util\DictionaryBase;

use function Spinit\Util\arrayGet;
use function Spinit\Util\asArray;
use function Spinit\Util\getInstance;

class Article {
    private $site;
    private $dat;
    private $links;
    private $ctr;

    public function __construct($site, $dat) {
        $this->site = $site;
        $this->id_lng = $this->site->getInstance()->getInfo('id_lng');
        if (!$this->id_lng) {
            //$dat['pat'] = $dat['lng'].'/'.$dat['pat'];
        }
        $this->dat = new DictionaryBase($dat);
        $this->ctr = getInstance($this->get('ctr', __NAMESPACE__.'\\ArticleController', 1), $this);
        $this->ctr->check();

        $this->makeLinks();
        $this->site->getInstance()->getResponse()->set('page.links', $this->links);
        $this->dat['body'] = $this->ctr->makeBody();
    }

    public function getSite() {
        return $this->site;
    }
    public function getDataSource($name = '') {
        return $this->site->getInstance()->getDataSource($name);
    }
    public function getLinks() {
        $args = func_get_args();
        if (!count($args)) {
            return $this->links;
        }
        return arrayGet($this->links, $args[0]);
    }
    public function get() {
        $args = func_get_args();
        if (!count($args)) {
            return $this->dat;
        }
        return call_user_func_array([$this->dat, 'get'], $args);
    }
    public function set() {
        $args = func_get_args();
        return call_user_func_array([$this->dat, 'set'], $args);
    }
    
    public function asArray() {
        return $this->dat->asArray();
    }

    public function getItemList($name, $opt = '') {
        return [];
    }

    private function makeLinks() {
        $links = new ArticleMakeLink($this);
        $this->links = $links->getLinks();
    }
}
