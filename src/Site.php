<?php

namespace Spinit\Dev\Opensite;

use Spinit\Dev\MRoute\Core\Assets;
use Spinit\Dev\MRoute\Core\HasInstance;
use Spinit\Dev\MRoute\Core\Type\InstanceInterface;
use Spinit\Dev\MRoute\Helper\ResourceLoader;
use Spinit\Dev\Opensite\Helper\ArticleFromLink;
use Spinit\Dev\Opensymap\Helper\FileNotFoundImage;
use Spinit\Util\Error\NotFoundException;
use Webmozart\Assert\Assert;

use function Spinit\Util\arrayGet;
use function Spinit\Util\asArray;

class Site {
    use HasInstance;
    
    private $name;
    private $dat;
    private $manifest;
    private $lang;
    private $pageLinks = [];

    public function __construct(InstanceInterface $instance, $name) {
        $this->setInstance($instance);
        if (is_array($name)) {
            $name['dat_del__'] = null;
            $this->dat = $this->getDataSource()->select('opn_sit', $name)->first();
        } else {
            $this->dat = $this->getDataSource()->select('opn_sit', ['nme'=>$name, 'dat_del__'=>null])->first();
        }
        @Assert::isArray($this->dat, "Sito non trovato [{$name}]");
        $this->name = $this->dat['nme'];
        $this->manifest = new Manifest($this);
    }
    public function getName() {
        return $this->get('nme');
    }
    public function get($field) {
        return arrayGet($this->dat, $field);
    }
    public function getPage() {
        $page = new Page($this, $this->getInstance()->getPath());
        return $page;
    }
    public function setLang($lang) {
        $this->lang = $lang;
    }
    public function getLang() {
        $args = func_get_args();
        if (!count($args)) {
            return $this->lang;
        }
        return arrayGet($this->lang, $args[0]);
    }

    public function getResourceInfo($name) {
        $base = $this->getResourceRoot();
        $rDir = $this->getInstance()->getHome($base);
        $rf = new ResourceLoader($rDir);
        return $rf->getResource($name);
    }

    public function showResource($args, $pag = [], $id_lng = '') {
        $args = asArray($args, '/');
        $trunk = array_shift($args);
        switch($trunk) {
            case 'res':
                $this->checkAccessPage($pag);
                try {
                    $name = array_shift($args);
                    $rr = $this->getResourceInfo($name);
                    $this->getInstance()->getResponse()->setContent(function() use($rr, $args) { 
                        $dim = [];
                        if (count($args) and $args[0]=='dim') {
                            array_shift($args);
                            $dim = explode('x',array_shift($args));
                        }
                        $rr['load'](array_shift($dim), array_shift($dim));
                    }, $rr['type']);
                } catch (NotFoundException $e) {
                    $notFound = new FileNotFoundImage($this->getInstance());
                    $notFound->show();
                }
                break;
        
            case 'theme':
                $asset = new Assets($this->getInstance(), fspath($this->getThemeRoot(), 'html'));
                $this->getInstance()->getResponse()->setContent($asset->run($args));
                break;
            case 'preview':
                $this->checkAccessPage($pag);
                $this->getResourceDoc($pag, $args, 'ref-doc-preview', $id_lng);
                break;
            case 'doc':
            case 'sec':
                $this->checkAccessPage($pag);
                $this->getResourceDoc($pag, $args, 'ref-doc-dowload', $id_lng);
                break;
            default:
                $notFound = new FileNotFoundImage($this->getInstance());
                $notFound->show();
                break;
        }
    }
    private function getResourceDoc($pag, $args, $type, $id_lng) {
        $sql = "
            select osip.val_str res
            from opn_sit_itm p
            inner join opn_sit_itm s on (s.id_par = coalesce(p.id_fst, p.id) and 
                                        s.id_sit = p.id_sit and 
                                        s.dat_del__ is null and 
                                        s.nme = {{sec}})
            inner join osy_itm st on (s.id_typ = st.id and st.urn = 'urn:opensite.org/item@type#box')
            inner join opn_sit_itm d on (d.id_par = coalesce(s.id_fst, s.id) and 
                                        d.id_sit = p.id_sit and 
                                        d.dat_del__ is null and 
                                        d.id_lng is null and
                                        d.nme = {{doc}})
            left join opn_sit_itm dd on (dd.id_par = d.id_par and 
                                        dd.id_sit = p.id_sit and
                                        dd.id_fst = d.id and
                                        dd.id_typ = d.id_typ and
                                        d.dat_del__ is null and 
                                        dd.id_lng = coalesce({{@lng}}, p.id_lng))
            inner join osy_itm dt on (d.id_typ = dt.id and dt.urn = 'urn:opensite.org/item@type#doc')
            inner join opn_sit_itm__p osip on (osip.id_mst = coalesce(dd.id, d.id) and osip.nme_prp = '{$type}')
            where p.id = {{@id}}
            and p.id_sit = {{@id_sit}}
        ";
        Assert::keyExists($pag, 'id');
        Assert::keyExists($pag, 'id_sit');
        $pag['sec'] = array_shift($args);
        $pag['doc'] = array_shift($args);
        $pag['lng'] = $id_lng;
        $list = $this->getDataSource()->query($sql, $pag);
        $doc = $list->first();
        $res = json_decode($doc['res'], 1);
        $newPath = array_merge(asArray(arrayGet($res, 'file'), '/'), $args);
        $this->showResource($newPath, $pag, $id_lng);
    }

    public function checkAccessPage($pag) {
        $acc = new Helper\AccessPage($this->getInstance(), $pag);
        if(!$acc->check()) {
            throw new Helper\AccessException();
        }
    }
    /**
     * Se è installato un tema ... viene usato.
     * Altrimenti viene usato quello di default
     */
    public function getThemeRoot($path = '') {
        // esiste un tema impostato sull'istanza?
        $root = fspath(getcwd(), '../opensite/theme', arrayGet($this->dat, 'thm', 'default'));
        if (!is_dir($root)) {
            $root = $this->getInstance()->getHome(fspath('opensite/theme', arrayGet($this->dat, 'thm', 'default')));
        }
        if (!is_dir($root)) {
            // no ... allora viene impostato quello di default di opensite
            $root = fspath(dirname(__DIR__), 'theme');
        }
        return fspath($root, $path);
    }
    
    public function getResourceRoot() {
        return "opensite/upload/{$this->dat['id']}/";
    }

    /**
     * Ricerca tutti gli elementi della sezione indicata nel path o l'elemento indicato
     */
    public function getSectionItem($lang, $path) {
        $part = asArray($path, '/');
        $sec = array_shift($part);
        $itm = array_shift($part);
        $cmd = "
            select hex(ii.id) as id_fst, hex(iil.id_lng) as id_lng, coalesce(iil.dsc, ii.dsc) as dsc
            from opn_sit_itm s
            inner join osy_itm ts on (s.id_typ = ts.id and ts.urn = 'urn:opensite.org/item@type#box')
            inner join opn_sit_itm ii on (ii.id_sit = s.id_sit and ii.id_par = s.id and ii.id_lng is null and ii.dat_del__ is null and
                                          ii.nme = case when {{itm}} = '' then ii.nme else {{itm}} end )
            inner join osy_itm ti on (ii.id_typ = ti.id and ti.urn = 'urn:opensite.org/item@type#doc')
            left join opn_sit_itm iil on (iil.id_fst = ii.id and iil.id_lng = {{@lng.id}} and {{@lng.id}} is not null and iil.dat_del__ is null)
            where s.dat_del__ is null
              and s.id_sit = {{@id_sit}}
              and s.id_lng is null
              and s.id_par is null
              and s.nme = {{sec}}
        ";
        $list = $this->getDataSource()->query($cmd, [
            'sec'=>$sec, 
            'itm'=>$itm, 
            'id_sit'=>$this->get('id'),
            'lng'=>$lang]);
        //debug($list);
        return $list;
    }

    /**
     * Ricerca la pagina indicata nel path
     */
    public function getArticle($lang, $path) {
        $cmd = new ArticleFromLink($this);
        $art = $cmd->getArticle($path, $lang);
        $this->checkAccessPage(['id'=>$art->get('id'), 'id_sit'=>$art->get('id_sit')]); 
        return $art;
    }

    public function getManifest() {
        return $this->manifest;
    }

    /**
     * Da un sito è possibile richiamare una url di un altro sito .. se appartenente allo stesso gruppo
     */
    public function getPagePath($siteName, $pageName, $lang) {
        if (!$siteName) {
            $siteName = $this->getInstance()->getEnv('opensite:name');
        }
        $group = $this->getInstance()->getEnv('opensite:group');
        if (!array_key_exists($siteName, $this->pageLinks)) {
            $this->pageLinks[$siteName] = new Helper\SiteLinks($this->getInstance(), $group, $siteName, $lang);
        }
        return $this->pageLinks[$siteName]->get($pageName, '--not-found--', 0);
    }

    public function makePath() {
        $args = array_map(function($item) {return trim($item, '/');}, func_get_args());
        $siteName = $this->getInstance()->getEnv('opensite:name');
        $group = $this->getInstance()->getEnv('opensite:group');
        if (!array_key_exists($siteName, $this->pageLinks)) {
            $this->pageLinks[$siteName] = new Helper\SiteLinks($this->getInstance(), $group, $siteName, $this->getLang('id'));
        }
        return $this->pageLinks[$siteName]->make(implode('/', $args));
    }

}
