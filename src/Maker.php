<?php

namespace Spinit\Dev\Opensite;

use function Spinit\Util\asArray;

abstract class Maker {

    private $page;

    public function __construct(Page $page) {
        $this->page = $page;
    }

    public function getPage() {
        return $this->page;
    }

    public function getSite() {
        return $this->getPage()->getSite();
    }
    public function getInstance() {
        return $this->getSite()->getInstance();
    }

    public function getDataSource($name = '') {
        return $this->getInstance()->getDataSource($name);
    }
    
    abstract public function make($param, $args = [], $cdata = '', $extra =[]) ;

    public static function process($param, $cnt, $funcList) {
        $make = function($extra) use ($param, $funcList) {
            $lines = explode("\n", array_shift($extra));
            $args = asArray(array_shift($lines), ' ');
            $op = array_shift($args);
            $str = "<code>-- {$op} : non implementato --</code>";
            $last = array_pop($extra);
            foreach ([$op, '*'] as $field) {
                if (!array_key_exists($field, $funcList)) {
                    continue;
                }
                $str = call_user_func_array($funcList[$field], [$param, $args, implode("\n", $lines), $extra]);
                break;
            }
            return $str.implode(':]]', $part).$last;
        };
        $str = '';
        foreach(asArray($cnt, '[[:') as $k => $part) {
            if ($k) {
                $str .= $make(asArray($part,':]]'));
            } else {
                $str .= $part;
            }
        }
        return $str;
    }
}