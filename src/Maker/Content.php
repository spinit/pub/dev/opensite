<?php

namespace Spinit\Dev\Opensite\Maker;

use Spinit\Dev\Opensite\Article;
use Spinit\Dev\Opensite\Maker;

use function Spinit\Util\arrayGet;
use function Spinit\Util\asArray;
use function Spinit\Util\getInstance;
use function Spinit\Util\normalize;

class Content extends Maker {

    public function make($param, $args = [], $cdata = '', $extra = []) {
        return self::process($param, $cdata, [
            'url' => [$this, 'ProcessUrl'], 
            'echo' => [$this, 'ProcessEcho'], 
            'make' => [$this, 'ProcessMake'], 
            'items' => [$this, 'ProcessItems'], 
            'manifest' => [$this, 'ProcessManifest'],
            '*'     => [$this, 'ProcessDefault'], 
            'sect' => [$this, 'ProcessSection'],
            'foreach'=>[$this, 'ProcessForeach'],
            'is' => [$this, 'ProcessIsArticle'],
            'bread' => [$this, 'ProcessBread']
        ]);
    }

    protected function ProcessUrl($param, $arg, $cdata) {
        $fname = array_shift($arg);
        $path = fspath($this->getSite()->getThemeRoot(), $fname);
        $url = $this->getInstance()->makePath($fname);
        if (is_file($path)) {
            $url .= '?v='.sha1_file($path);
        }
        return $url;
    }

    protected function ProcessEcho($param, $args, $cdata) {
        $key = array_shift($args);
        if (is_array($param)) {
            return arrayGet($param, asArray($key, '.'));
        }
        return $param->get($key);
    }

    protected function ProcessItems($param, $args, $cdata) {
        $name = array_shift($args);
        $opt = array_shift($args);
        $str = array_shift($args);
        if ($param instanceof Article) {
            $itemList = $param->getItemList($name, $opt);
            foreach($itemList as $item) {
                list($cnt, ) = normalize($cdata, $item);
                $str.= $cnt;
            }
        }
        return $str;
    }

    protected function ProcessMake($param, $arg, $cdata, $extra) {
        try {
            $obj = getInstance(array_shift($arg), $this->getPage());
            $jarg = json_decode(implode(' ', $arg), 1);
            return $obj->make($param, $jarg?:$arg, $cdata, $extra);
        } catch (\Exception $e) {
            return 'Maker non trovato : <code>'.htmlspecialchars($e->getMessage()).'</code>';
        }
    }

    protected function ProcessManifest($param, $arg, $cdata) {
        $manifest = $this->getSite()->getManifest();
        $str = [''];
        $list = [];
        $iter = function($type) use ($manifest, &$list) {
            foreach($manifest->getAssetList($this->getPage()->getLayout(), $type) as $res) {
                if (!in_array($res, $list)) {
                    yield $res;
                    $list[] = $res;
                }
            }
        };
        switch(arrayGet($arg, 0)) {
            case 'css':
                foreach($iter('css') as $res) {
                    $str []= '<link rel="stylesheet" href="'.$manifest->makeUrl($res).'"/>';
                }
                break;
            case 'js':
                foreach($iter('js') as $res) {
                    $str []= '<script src="'.$manifest->makeUrl($res).'" type="application/javascript"></script>';
                }
                break;
        }
        return implode("\n    ", $str)."\n";
    }

    protected function ProcessDefault($param, $args, $cdata) {
        return '';
    }

    protected function processSection($param, $args, $cdata) {
        $cnt = '';
        foreach($this->getSite()->getSectionItem($this->getSite()->getLang(), array_shift($args)) as $item) {
            $cnt .= $this->make($param, [], $item['dsc']);
        }
        return $cnt;
    }

    protected function ProcessForeach($param, $args, $cdata) {
        $str = '';
        $cmd = array_shift($args);
        foreach($param->get($cmd) as $item) {
            list($cnt, ) = normalize($cdata, $item);
            $str .= $cnt;
        }
        return $str;
    }

    protected function ProcessIsArticle($param, $args, $cdata) {
        $name = array_shift($args);
        if ($param->get('page.name') == $name) {
            return ' '.array_shift($args);
        }
        return '';
    }

    protected function ProcessBread($art, $args, $cdata, $extra) {
        $sep = array_shift($args);
        $mask = trim(implode(' ', $args)."\n".$data);
        $site = $this->getSite();
        $cmd = "
            select fnme, hex(id) as id, lbl
            from opn_sit_itm p
            where p.dat_del__ is null
              and p.id_sit = {{@id_sit}}
              and p.id_typ = {{@id_typ}}
              and p.id = coalesce(p.id_fst, p.id)
              and locate(p.fnme, {{fnme}})
            order by fnme
        ";
        $list = $site->getDataSource()->query($cmd, [
            'fnme'=>$art->get('fnme'), 
            'id_sit'=>$art->get('id_sit'), 
            'id_typ'=>$art->get('id_typ')
        ]);
        foreach($list as $item) {
            if ($item['fnme'] == $art->get('fnme')) continue;
            list($cnt, ) = normalize($mask, [
                'link'=>$site->getPagePath('', $item['fnme'], $art->get('id_lng')),
                'name'=>$item['lbl']]);
            $bread []= $cnt;
        }
        foreach($extra as $mask) {
            list($cnt, ) = normalize($mask, [
                'link'=>$site->getPagePath('', $art->get('fnme'), $art->get('id_lng')),
                'name'=>$art->get('lbl')]);
            $bread []= $cnt;
        }
        return " ".implode(" {$sep} ", $bread)." ";
    }
}
