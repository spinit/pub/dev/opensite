<?php
$model = $this->getApplication()->getModel('Admin:Env');
$pkey = ['id_url'=>$this->getModel()->get('id'), 'nme'=>'opensite:lang'];
$lng = $this->getField('url_lng');
$model->load($pkey);
$model->set($pkey);
if ($lng->getValue('id')) {
    $model->set('val', $lng->getValue('id'));
    $model->set('dsc', $lng->getValue('name'));
    $model->save();
} else {
    $model->delete();
}
