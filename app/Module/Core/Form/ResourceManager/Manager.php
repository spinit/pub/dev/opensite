<?php

namespace Spinit\Dev\Opensite\App\Module\Core\Form\ResourceManager;

use Spinit\Dev\Opensymap\App\System\Module\Core\Form\ResourceManager\Manager as OsyManager;
use Spinit\Dev\Opensite\Site;

class Manager extends OsyManager {

    protected function initPath($path) {
        $path = parent::initPath($path);
        $pkey = json_decode(base64_decode($this->getPkey()), 1);
        $this->site = new Site($this->getInstance(), $pkey);
        return $path;
    }

    public function getHome($path = '') {
        $home = $this->getInstance()->getHome(fspath($this->site->getResourceRoot(), $path));
        return $home;
    }

    protected function getContext() {
        $pkey = json_decode(base64_decode($this->getPkey()), 1);
        return 'opensite:'.$pkey['id'];
    }
}