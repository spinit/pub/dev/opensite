<?php
use Spinit\Dev\Opensymap\Core\Helper\FieldFormDetail;
$this->set('formDetail', 'Core:SiteItemPageDownload');
$this->set('formData', [
    'hdn_par'=>'hdn_id', 
    'hdn_sit'=>'hdn_sit', 
    'src_lng'=>'src_lng'
]);
$this->set('formValue', [
    'hdn_fst'=>$event->getParam(0), 
]);
$ff = new FieldFormDetail($this);
$ff->exec();