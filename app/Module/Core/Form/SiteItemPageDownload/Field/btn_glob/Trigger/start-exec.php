<?php
$model = $this->getForm()->getModel();
if ($model->get('id_lng') and $model->get('id_fst')==$model->get('id')) {
    $model->set('id_lng', '');
    $model->save();
}
$this->getForm()->getResponse()->addCommand('back');