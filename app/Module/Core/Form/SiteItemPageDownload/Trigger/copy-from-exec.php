<?php
use Spinit\Util\Tag;

$form  = $this;
$pag = ['id'=>$form->getField('hdn_fst')->getValue(), 'id_sit'=>$form->getField('hdn_sit')->getValue()];
if ($id_pag = $event->getParam(0)) {
    // invio del contenuto da impostare
    $cmd = "
        select lay, dsc, lbl
        from opn_sit_itm p
        where p.id = {{@id}} 
        and p.id_sit = {{@id_sit}}";
    $pag['id'] = $id_pag;
    $rec = $form->getDataSource()->query($cmd, $pag)->first();
    
    foreach(['lbl'=>'txt_ttl', 'lay'=>'txt_lbl', 'dsc'=>'txt_dsc'] as $f=>$c) {
        $el = $form->getField($c);
        $el->setValue($rec[$f], 1);
    }
    $form->getResponse()->addCommand('$(".box-content").trigger("close")');
} else {
    // visualizzazione delle possibili scelte
    $cmd = "
        select hex(p.id) as id, case when p.id_lng is null then '--Default--' else l.nme end as nme, hex(l.id) as id_lng
        from opn_sit_itm p
        left join osy_itm l on (p.id_lng = l.id)
        where p.id_fst = {{@id}} 
          and p.id_sit = {{@id_sit}}
          and p.dat_del__ is null
          and coalesce(p.dsc, '') != ''
        order by l.nme";
    $rs = $form->getDataSource()->query($cmd, $pag);
    $div = new Tag('div');
    foreach($rs as $itm) {
        if ($itm['id_lng'] == $form->getField('src_lng')->getValue('id')) continue;
        $div->add(new Tag('div'))
            ->att('class', 'osy-item')
            ->att('event', 'copy-from')
            ->att('data-param', $itm['id'])
            ->add($itm['nme']);
    }
    $form->getResponse()->addBox('this', $div);
}
