<?php
use Spinit\Dev\Opensite\Site;

$model = $this->getModel();

$form = $this->getApplication()->getForm('Core:SiteItemPage');
$form->setPkey([
    'id'=>$this->getField('hdn_par')->getValue(),
    'id_sit'=>$this->getField('hdn_sit')->getValue()
]);
$base = $form->getUrlPath(1);
$site = new Site($this->getInstance(), ['id'=>$this->getField('hdn_sit')->getValue()]);

$id_fst = $this->getField('hdn_fst')->getValue();
$id_itm = $this->getField('hdn_id')->getValue();

$lng = $this->getField('src_lng');

$this->getField('txt_dsc')->set('content-css', $base.'style.css');
$this->getField('txt_dsc')->set('content-js', $base.'script.js');
$this->getField('txt_dsc')->set('base-url', $base.'base-url/'.($lng->getValue('id')?:'-').'/');
$this->getField('txt_dsc')->set('tabs', [["title"=>"Resource", "url"=>$base.'res/']]);

if ($id_fst != $id_itm) {
    if ($model->get('id')) {
        $lng->setValue($model->get('id_lng'));
    }
    $fst = $this->getDatasource()->select('opn_sit_itm', $id_fst, 'nme')->first();
    $this->getField('lbl_nme')->setValue($fst['nme']);
    $this->getField('lbl_nme')->set('visible', '');
    $this->getField('txt_nme')->set('visible', 'no');
}
$this->getField('lbl_lng')->setValue($lng->getValue('name'));

$model = $this->getModel();
if (!$model->get('id_lng') or $model->get('id_fst')!=$model->get('id')) {
    $this->getField('pnl_glob')->set('visible', 'no');
}
