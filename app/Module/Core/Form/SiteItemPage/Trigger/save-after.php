<?php

// viene chiesto al pannello attuale di salvare i suoi dati correnti
$this->getField($this->getField('tab_info')->getValue())->trigger('@save');
$tmpl = $this->getField('txt_tmpl_item');

// salvataggio nome pagina totale
$idpar = $this->getField('hdn_par')->getValue();
$model = $this->getModel();

$fnme = $model->get('fnme');
if (!$idpar) {
    $model->set('fnme', $model->get('nme'));
} else {
    $pnme1 = $this->getDataSource()->select('opn_sit_itm', $idpar, 'fnme')->first('fnme');
    $model->set('fnme', $pnme1.'/'.$model->get('nme'));
}
$fnme1 = $model->get('fnme');
if ($fnme1 != $fnme) {
    $model->save();
    $this->getDataSource()->exec("
        update opn_sit_itm
        set fnme = concat({{fnme1}}, substr(fnme, length({{fnme}})+1))
        where id_sit = {{@id_sit}}
	  and id_typ = {{@id_typ}}
          and substr(fnme, 1, length({{fnme}})+1) = '{{:fnme}}/'
    ", ['fnme'=>$fnme, 'fnme1'=>$fnme1, 'id_sit'=>$model->get('id_sit'), 'id_typ'=>$model->get('id_typ')]);
}


