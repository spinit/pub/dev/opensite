<?php
$model = $this->getModel();

$arg = [
    'id_sit'=>$model->get('id_sit'),
    'id_typ'=>$model->get('id_typ')
];

$deleteChilds = function ($listParent) use ($arg, &$deleteChilds) {
    var_dump(['parent', $listParent]);
    if (count($listParent) == 0) {
        return;
    }
    $par = array_merge($arg, ['ids_pag'=>$listParent]);
    // cancella le pagine in cascata
    $listChilds = $this->getDataSource()->query('
        select hex(id) as id 
        from opn_sit_itm d
        where id_sit = {{@id_sit}}
        and id_par in ({{@+ids_pag}})
        and id_typ = {{@id_typ}}
        and id = coalesce(id_fst, id)
        -- and dat_del__ is null', $par)->getList();
    $deleteChilds($listChilds);
    if (count($listChilds)) {
        $this->getDataSource()->delete('opn_sit_itm', ['id'=>$listChilds]);
        var_dump(['childs', $listChilds]);
    }
};

$deleteChilds([$model->get('id')]);

