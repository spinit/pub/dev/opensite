<?php
use Spinit\Dev\Opensite\Site;

//debug($this->getPkey(), $this->getModel()->asArray());
$base = $this->getUrlPath(1);
$site = new Site($this->getInstance(), ['id'=>$this->getField('hdn_sit')->getValue()]);

$par = $this->getDataSource()->select('opn_sit_itm', $this->getField('hdn_par')->getValue(), 'fnme')->first();

if ($par['fnme']) {
    $this->getField('lbl_par')->setValue("Genitore : <code> ".str_replace('/', ' &raquo; ', $par['fnme'])."</code>");
}

$lng = $this->getField('src_lng');
$lng->trigger('@check');
$this->getField('txt_dsc')->set('content-css', $base.'style.css');
$this->getField('txt_dsc')->set('content-js', $base.'script.js');
$this->getField('txt_dsc')->set('base-url', $base.'base-url/'.($lng->getValue('id')?:'-').'/');
$this->getField('txt_dsc')->set('tabs', [["title"=>"Resource", "url"=>$base.'res/']]);
