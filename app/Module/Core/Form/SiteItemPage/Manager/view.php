<?php        
    $form = $this->getForm();
    $form->getResponse()->setHeader('Content-Type', 'text/html');
?>
<!DOCTYPE html>
<html>
<head>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
    <style>
    .label {display:inline-block}
    </style>
</head>
<body>
<div class="container-fluid">
    <div class="row">
        <div class="col-md-12">
            <input type="text" name="q" id="q"/>
            <button type="button" class="btn btn-info btn-sm">Cerca</button>
        </div>
    </div>
    <div class="row" id="main"></div>
</div>
<script src="https://code.jquery.com/jquery-3.5.1.min.js" crossorigin="anonymous"></script>
<script src="<?php echo $form->getInstance()->makePath('assets/app/desktop.js')?>"></script>
<script src="<?php echo $form->getInstance()->makePath('assets/app/app.js')?>"></script>
<script>
$('#main').on('setResource', function(event, list) {
    let main = this;
    list = list ? list : [];
    main.innerHTML = '';
    list.forEach(item => {
        let c = document.createElement('div');
        c.className = 'col '+item.class;
        c.innerHTML = item.content;
        main.insertAdjacentElement('beforeend', c);
    });
});
$(function() {
    $('#main').trigger('sendEvent', ['par', {'event':'init'}]);
});
</script>
</body>
</html>
