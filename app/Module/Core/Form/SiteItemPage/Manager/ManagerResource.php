<?php
namespace Spinit\Dev\Opensite\App\Module\Core\Form\SiteItemPage\Manager;

use Spinit\Dev\Opensymap\Core\Type\FormController;
use Spinit\Dev\Opensymap\App\System\Module\Core\Form\ResourceManager\Source\ResourceList;

use function Spinit\Util\arrayGet;

class ManagerResource extends FormController {

    public function run() {
        if ($event = $this->getForm()->getStorage('sys.osy.event')) {
            $methodEvent = 'on'.ucfirst($event);
            if (method_exists($this, $methodEvent)) {
                return call_user_func_array([$this, $methodEvent], []);
            }
            throw new \Exception('Evento non gestito '.$event );
        }
        include (__DIR__.'/view.php');
    }

    protected function onInit() {
        
        $rs = new ResourceList($this->getDataSource());
        $pkey = $this->getForm()->getPkey();
        $list = [];
        foreach($rs->getList(arrayGet($pkey, 'id'), arrayGet($pkey, 'id_sit')) as $item) {
            $list []= ['class'=>'col-md-3', 'content'=>json_encode($item)];
        };
        if (!count($list)) {
            $list []= ['class'=>'col-md-12', 'content'=>'Nessun elemento trovato'];
        }
        $this->getResponse()->addTrigger('main','setResource', $list);
    }
}