<?php
use Spinit\Dev\Opensite\Site;

$rec = $this->getDataSource()->query("
    select s.nme, p.lay,
        (select val_str from opn_sit_itm__p pp
         where pp.id_mst = p.id 
           and pp.nme_prp = 'urn:opensite.org:page:property#style'
         limit 1) sty
    from opn_sit_itm p 
    inner join opn_sit s on (p.id_sit = s.id)
    where p.id = {{@id}}", $this->getPkey())->first();
$site = new Site($this->getInstance(), $rec['nme']);
$manifest = $site->getManifest();
$manifest->echoAllCss($rec['lay']);
echo "

/*
 * INNER STYLE
 */

{$rec['sty']}

";
