<?php

use Spinit\Dev\MRoute\Core\Assets;
use Spinit\Dev\MRoute\Core\Response;
use Spinit\Dev\MRoute\Helper\ResourceLoader;
use Spinit\Dev\Opensite\Site;
use Spinit\Dev\Opensymap\Helper\FileNotFoundImage;
use Spinit\Util\Error\NotFoundException;

use function Spinit\Util\arrayGet;
use function Spinit\Util\asArray;

$args = asArray($asset->getArgPath(), '/');
$id_lng = array_shift($args);
$site = new Site($this->getInstance(), ['id' => arrayGet($this->getPkey(), 'id_sit')]);
//debug($args);
$site->showResource($args, $this->getPkey(), $id_lng);
