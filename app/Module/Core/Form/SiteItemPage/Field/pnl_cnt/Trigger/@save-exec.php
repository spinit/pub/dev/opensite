<?php
// salvataggio dati

use function Spinit\Util\arrayGet;

$form = $this->getForm();
// dati correnti
$data = $form->getModel();
// canale di salvataggio

$tmpl = $form->getField('txt_tmpl_item');

// se la lingua è uguale a quella del master ... i dati vengono caricati sul master
$id_lng = $form->getField('src_lng')->getValue('id');
if ($data->get('id_lng') == $id_lng) {
    $model = $data;
} else {
    // altrimenti occorre caricare i dati su un record secondario
    $model = $form->getApplication()->getModel('Core:SiteItem');
    $model->load(['id'=>$data->get('id'), 'id_fst'=>$data->get('id'), 'id_lng'=>$id_lng, 'dat_del__'=>null]);
    // se non è una pagina principale ... viene controllato se non è una traduzione
    if (!$model->get('id')) {
        $model->load(['id_fst'=>$data->get('id'), 'id_lng'=>$id_lng, 'dat_del__'=>null]);
        if ($model->get('id')) {
            $model->setPkey(['id'=>$model->get('id')]);
        }
    }
    $model->set('id_par', $data->get('id_par'));
    $model->set('id_sit', $data->get('id_sit'));
    $model->set('id_fst', $data->get('id'));
    $model->set('id_typ', $data->get('id_typ'));
    $model->set('id_lng', $id_lng);
}
$model->set('dsc', $form->getField('txt_dsc')->getValue());
$model->set('lbl', $form->getField('txt_ttl')->getValue());
$model->set('slug', $form->getField('txt_slug')->getValue());
$model->set('dis', $form->getField('chk_dis')->getValue());
$model->set('dfl', $form->getField('chk_dfl')->getValue());
$model->setPropertyValue($tmpl->get('property-item'), $tmpl->getValue());
//debug($model->asArray());
$model->save();

$this->getForm()->getResponse()->set('init.hdn_pag', $model->get('id'));
