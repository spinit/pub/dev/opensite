<?php

use function Spinit\Util\arrayGet;

$form = $this->getForm();
$tmpl = $form->getField('txt_tmpl_item');
$lng  = $form->getField('src_lng');
$model = $form->getModel();
$rec = [];
if ($model->get('id')) {
    if (!$lng->getValue('id')) {
        $lng->setValue('id', $model->get('id_lng'));
    }
    $cmd = "
        select hex(id) as id, d.nme, d.dsc, d.slug, d.lbl, d.dis, d.dfl,
            (select val_str from opn_sit_itm__p where id_mst = d.id and nme_prp = {{prp}} limit 1) as tmpl
        from opn_sit_itm d
        where d.id_fst = {{@id}}
        and d.id_sit = {{@id_sit}}
        and d.id_lng = {{@id_lng}}
        and d.dat_del__ is null
    ";
    $dat = $model->getPkey();
    $dat['id_lng'] = $lng->getValue('id');
    $dat['prp'] = $tmpl->get('property-item');
    $rec = $form->getDataSource()->query($cmd, $dat)->first();
    //debug($rec);
}
$this->getForm()->getResponse()->set('init.hdn_pag', $model->get('id'));

$form->getField('hdn_pag')->setValue(arrayGet($rec, 'id', $model->get('id')), 1);
$form->getField('txt_ttl')->setValue(arrayGet($rec, 'lbl'), 1);
$form->getField('txt_slug')->setValue(arrayGet($rec, 'slug'), 1);
$form->getField('txt_dsc')->setValue(arrayGet($rec, 'dsc'), 1);
$form->getField('chk_dis')->setValue(arrayGet($rec, 'dis'), 1);
$form->getField('chk_dfl')->setValue(arrayGet($rec, 'dfl'), 1);
$tmpl->setValue(arrayGet($rec, 'tmpl'), 1);

