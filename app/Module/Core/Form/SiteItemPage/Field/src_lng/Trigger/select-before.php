<?php
// quando cambia la lingua ... occorre salvare la pagina
$form = $this->getForm();
$form->trigger('save');
if ($form->getResponse()->hasError()) {
    $form->getResponse()->throwException();
}