<?php
namespace Spinit\Dev\Opensite\App\Module\Core\Form\SiteItemPage\Controller;

use Spinit\Dev\Opensymap\Core\Type\FieldController;
use Spinit\Util;

class RisorseController extends FieldController {
    public function makeTranslateButton($val, $rec, $col) {
        if ($rec['id_cld']) {
            return '';
        }
        $event = Util\arrayGet($col, 'event');
        $btn = '<span class="btn btn-outline-info btn-sm" event="'.$event.'" data-param="'.$rec['id'].'"> &raquo; </span>';
        return $btn;
    }
}