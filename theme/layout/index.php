<!DOCTYPE html>
<html lang="[[:echo page.lang.name :]]">
<head>
    <base href="[[:echo page.base :]]"/>
    <meta name="MobileOptimized" content="320" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <link rel="shortcut icon" href="[[:echo page.asset:]]/img/favicon.ico?[[:echo page.lang.name :]]" />
    <!--<meta name="viewport" content="width=device-width">-->
    [[:manifest css:]]
    <style>
[[:echo page.style :]]
    </style>
</head>
<body>
    <nav id="menu">[[:sect menubar/main:]]</nav>
    <div id="content">[[:echo page.content:]]</div>
    <div id="footer">[[:sect footer/main:]]</div>
    [[:manifest js:]]
</body>
</html>