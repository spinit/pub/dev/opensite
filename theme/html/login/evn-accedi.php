<?php

$user = $this->getInstance()->getCommand('User:Autentication', $this->getInstance())
        ->exec($this->getBody('txt_login'), $this->getBody('txt_passwd'));

if (!$user) {
    throw new \Exception('Credenziali non valide');
}
$this->getInstance()->setUser($user);
// viene effettuato un eventuale aggiustamento alla url di richiesta per poter
// pescare i cookie nel giusto path
$this->getResponse()->addCommand('location.replace(args[0])', '//'.rtrim($this->getInstance()->getRequestUri(), '/') .'/');
