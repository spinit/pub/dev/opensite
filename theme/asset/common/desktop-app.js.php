<?php

use Spinit\Dev\Opensymap\Helper\FileNotFoundImage;

$dname = realpath(__DIR__.'/../../../../dev-opensymap/theme/default/html/assets/app/desktop.js');
$aname = realpath(__DIR__.'/../../../../dev-opensymap/theme/default/html/assets/app/app.js');
if (is_file($dname) and is_file($aname)) {
    readfile($dname);
    readfile($aname);
} else {
    throw new FileNotFoundImage($dname.' OR '.$aname);
}